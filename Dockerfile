FROM node:14.17.3-stretch

ADD main.js /app/main.js

ENTRYPOINT [ "node", "/app/main.js" ]
